var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var VerifyToken = require('../auth/VerifyToken');
var config = require('../config'); // get our config file

requestjson = require('request-json');
var owmclient = requestjson.createClient('http://api.openweathermap.org/');









async function getWeather(db, id) {
	try {
        let owmbody = await owmclient.get('data/2.5/weather?id=' + id + '&APPID=86255ecc22d4a3befa2869e83ec2a8aa');

        if(owmbody.body.cod != 200) {
            throw new Error('Unexpected Response from Weather API');
        }

        let dbres = await db.collection("weathers").insertOne(owmbody.body);
        if (dbres.err) {
            console.log(dbres.err);
        } else {
            console.log("Weather document inserted");
        }

        return owmbody.body;
	} catch(e) { // Error in requesting from OpenWeatherMap
        console.log(e);
        let weatherResult = await getWeatherOffline(db, id);
        if(weatherResult) { // null when weather not found in db
            return weatherResult;
        } else {
            return { 'cod': 408, 'err': 'Request timeout to OpenWeatherMap' }
        }
	}
}

async function getWeatherOffline(db, id) {
    let dbres = await db.collection("weathers").findOne(
        { 'id': id }, // query
        { sort: { 'dt': -1 } }, // options to get newest date
    );
    return dbres; // Return null if no result found
}

async function login(username, password) {
    let users = { 'wilson' : '$2y$12$MB.bAsEam5Gxa.A7F9U58e4rWvQem9/7ydgeJWhV4gXKT0/2qDAoW' };
    // Password: wilsonch

    if(!users[username]) return { 'auth': false, 'token': null }; // User not found

    let passwordIsValid = await bcrypt.compare(password, users[username]);
    if (!passwordIsValid) return { 'auth': false, 'token': null }; // Wrong Password

    let token = jwt.sign({ 'username': username }, config.secret, {
        expiresIn: 86400 // expires in 24 hours
    });
    
    return { 'auth': true, 'token': token };
}










var appRouter = function(app) {
	
	app.get("/", function(req, res) {
		res.send("Hello World");
    });

	app.get("/weather", VerifyToken, async (req, res) => {
        let weatherResult = await getWeather(req.db, 8223932); // Get weather for Central, HK
        res.send(weatherResult);
    });

    app.get("/weatheroffline", VerifyToken, async (req, res) => {
        let weatherResult = await getWeatherOffline(req.db, 8223932); // Get weather for Central, HK
        res.send(weatherResult);
    });

    app.post("/login", async (req, res) => {
        let loginResult = await login(req.body.username, req.body.password);
        res.send(loginResult);
    });

};

module.exports = appRouter;