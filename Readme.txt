Readme for Weather API
Installing and Running App
Important note:
1. On weather-api/config.js, if running locally, change to
'mongodbserver': 'mongodb://localhost:27017/weatherdb'
How to install the app:
1. Install node.js and mongodb
2. Run "npm install"
How to run the app:
1. Run "npm start"
2. API is now available from http://localhost:3000 
How to install the app via docker
1. Install docker
2. Run "docker-compose up"

Tester Program:
Tester program is available at test/test.js
How to run tester program:
1. Run "npm install request-json"
2. Change hostname of test.js to match location of server
3. Run "node test.js"


API Documentation
POST /login
Input: JSON 
{
	"username": <username>,
	"password": <password>
}
Note: Correct username is "wilson" and password is "wilsonch"
Output: JSON
Correct username and password 
{
	"auth": true,
	"token": <token>
}
Wrong username and/or password
{
	"auth": false,
	"token": null
}


GET /weather
Input: Headers: 
Key: x-access-token
Value: <Access token from login process>

Output: JSON
Correct Access Token
{
	<weather data>
}
Wrong or No Access Token
{
	"auth": false,
	"message": <error message>
}
Request timeout and no previous weather data
{
	"cod": <error code>,
	"err": <error message>
}
