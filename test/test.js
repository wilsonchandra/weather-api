requestjson = require('request-json');
var wclient = requestjson.createClient('http://localhost:3000/');

let logindata = {"username":"wilson", "password":"wilsonch"};
wclient.post('login', logindata, function(err, res, body) {
    let token = body.token;

    if(body.auth && body.token) {
        console.log("Test Pass: Login - correct username and password");
    } else {
        console.log("Test FAIL: Login - correct username and password");
    }

    let wrongpass = {"username":"wilson", "password":"wilson"};
    wclient.post('login', wrongpass, function(err, res, body) {
        if(!body.auth && !body.token) {
            console.log("Test Pass: Login - correct username and wrong password");
        } else {
            console.log("Test FAIL: Login - correct username and wrong password");
        }
    });

    let wronguser = {"username":"wilsonch", "password":"wilsonch"};
    wclient.post('login', wronguser, function(err, res, body) {
        if(!body.auth && !body.token) {
            console.log("Test Pass: Login - wrong username and correct password");
        } else {
            console.log("Test FAIL: Login - wrong username and correct password");
        }
    });

    let wronguserpass = {"username":"wilsonch", "password":"wilson"};
    wclient.post('login', wronguserpass, function(err, res, body) {
        if(!body.auth && !body.token) {
            console.log("Test Pass: Login - wrong username and wrong password");
        } else {
            console.log("Test FAIL: Login - wrong username and wrong password");
        }
    });

    let nouser = {"password":"wilson"};
    wclient.post('login', nouser, function(err, res, body) {
        if(!body.auth && !body.token) {
            console.log("Test Pass: Login - no username");
        } else {
            console.log("Test FAIL: Login - no username");
        }
    });

    let nopass = {"user":"wilson"};
    wclient.post('login', nopass, function(err, res, body) {
        if(!body.auth && !body.token) {
            console.log("Test Pass: Login - no password");
        } else {
            console.log("Test FAIL: Login - no password");
        }
    });

    let nouserpass = {};
    wclient.post('login', nouserpass, function(err, res, body) {
        if(!body.auth && !body.token) {
            console.log("Test Pass: Login - no user and no password");
        } else {
            console.log("Test FAIL: Login - no user and no password");
        }
    });

    let blank = "";
    wclient.post('login', blank, function(err, res, body) {
        if(!body.auth && !body.token) {
            console.log("Test Pass: Login - blank");
        } else {
            console.log("Test FAIL: Login - blank");
        }
    });

    wclient.get('weather', function(err, res, body) {
        if(!body.auth) {
            console.log("Test Pass: Weather - no auth");
        } else {
            console.log("Test FAIL: Weather - no auth");
        }

        wclient.get('weather', function(err, res, body) {
            if(!body.auth) {
                console.log("Test Pass: Weather - no auth");
            } else {
                console.log("Test FAIL: Weather - no auth");
            }

            wclient.headers['x-access-token'] = token;
            wclient.get('weather', function(err, res, body) {
                if(body.cod == 200) {
                    console.log("Test Pass: Weather - valid auth");
                } else {
                    console.log("Test FAIL: Weather - valid auth");
                }
                wclient.get('weatheroffline', function(err, res, body) {
                    if(body.cod == 200) {
                        console.log("Test Pass: WeatherOffline - valid auth");
                    } else {
                        console.log("Test FAIL: WeatherOffline - valid auth");
                    }
                });
            });
        });
    });




    return console.log("Token: " + token);
});