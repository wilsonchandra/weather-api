var express = require("express");
var bodyParser = require("body-parser");
var http = require('http');
var expressMongoDb = require('express-mongo-db');
var config = require('./config'); // get our config file

var app = express();

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

app.use(expressMongoDb(config.mongodbserver));
// Change on config.js if using docker or locally

var routes = require("./routes/routes.js")(app);

var server = app.listen(3000, () => {
    console.log("Listening on port %s...", server.address().port);
});